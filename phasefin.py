import pygame
from pygame.locals import *

def create_scene_phase_fin(fenetre,score):
    font_score=pygame.font.Font(None,60)
    if score<=0:
        fondfin=pygame.image.load("fondfin2.png").convert()
    else:
        fondfin=pygame.image.load("fondfin.png").convert()
    fin=1
    while fin:
        fenetre.blit(fondfin,(0,0))
        if score<=0:
            fenetre.blit(font_score.render("Le cambriolage a echoue...",False,(0,60,150)),(400,100))
            fenetre.blit(font_score.render("Vous avez perdu "+str(abs(score))+" $ !",False,(0,60,150)),(400,200))
            fenetre.blit(font_score.render("Les reparations de l'helico ont coutee cher !",False,(0,60,150)),(250,300))
        elif score<=5000 and score>0:
            fenetre.blit(font_score.render("Vous etes revenu du cambriolage.",False,(0,60,150)),(325,100))
            fenetre.blit(font_score.render("Vous avez gagne "+str(score)+" $",False,(0,60,150)),(400,200))
            fenetre.blit(font_score.render("Il va falloir s'ameliorer.",False,(0,60,150)),(425,300))
        elif score<=15000 and score>5000:
            fenetre.blit(font_score.render("Vous avez un joli magot !",False,(0,60,150)),(400,100))
            fenetre.blit(font_score.render("Vous avez gagne "+str(score)+" $",False,(0,60,150)),(400,200))
            fenetre.blit(font_score.render("Vous vous en etes bien sorti !",False,(0,60,150)),(360,300))
        elif score<=20000 and score>15000:
            fenetre.blit(font_score.render("Vous etes devenu riche !",False,(0,60,150)),(400,100))
            fenetre.blit(font_score.render("Vous avez gagne "+str(score)+" $ !",False,(0,60,150)),(400,200))
            fenetre.blit(font_score.render("Felicitation, vous savez voler a la perfection !",False,(0,60,150)),(200,300))
        pygame.display.flip()

        for event in pygame.event.get():
            if event.type == QUIT or event.type == KEYDOWN and event.key == K_ESCAPE:
                fin = 0
