import pygame
from pygame.locals import *
import random
import math
from phase3 import *

class Perso(pygame.sprite.Sprite):
    def __init__(self,vie):
        pygame.sprite.Sprite.__init__(self)
        self.image=pygame.Surface([64, 42])
        self.frame1=(pygame.image.load("perso.png").convert_alpha()).copy()
        self.frame2=(pygame.image.load("perso2.png").convert_alpha()).copy()
        self.image=self.frame1
        self.image.convert_alpha()
        self.rect=self.image.get_rect()
        self.dx=0
        self.dy=0
        self.x=608
        self.y=581
        self.rect.x=self.x
        self.rect.y=self.y
        self.score=0
        self.k=0
        self.scale=1
        self.saut=True

    def update(self):
        if self.dy<5:
            self.dy+=0.5
        self.x+=self.dx
        self.y+=self.dy
        if self.y>581:
            self.y=580
            self.saut=True
        if self.x<608:
            self.x=608
            self.dx=0
        elif self.x>4512:
            self.x=4512
            self.dx=0
        self.rect.x=608
        self.rect.y=self.y

    def draw(self,fenetre):
        if self.dx!=0:
            self.k+=0.5
        if (self.scale>0 and self.dx<0) or (self.scale<0 and self.dx>0):
            self.frame1=pygame.transform.flip(self.frame1,True,False)
            self.frame2=pygame.transform.flip(self.frame2,True,False)
            self.scale*=-1

        if self.k<3:
            fenetre.blit(self.frame1,(608,self.y))
        else:
            fenetre.blit(self.frame2,(608,self.y))
        if self.k>6:
            self.k=0

class Laser(pygame.sprite.Sprite):
    def __init__(self,x,y,typel):
        pygame.sprite.Sprite.__init__(self)
        self.image=pygame.Surface([39, 23])
        self.image=(pygame.image.load("laser.png").convert_alpha()).copy()
        self.image.convert_alpha()
        self.rect=self.image.get_rect()
        self.x=x
        self.y=y
        self.type=typel
        self.k=0

    def draw(self,fenetre,perso):
        self.k+=0.1
        if self.type==0:
            self.rect.x=self.x-2*(perso.x-608)
            self.rect.y=self.y
            fenetre.blit(self.image, (self.x-2*(perso.x-608),self.y))
        elif self.type==1:
            self.rect.x=self.x-2*(perso.x-608)
            self.rect.y=self.y+50*math.cos(self.k)
            fenetre.blit(self.image, (self.x-2*(perso.x-608),self.y+50*math.cos(self.k)))
        elif self.type==2:
            self.rect.x=self.x-2*(perso.x-608)
            self.rect.y=self.y+50*math.sin(self.k)
            fenetre.blit(self.image, (self.x-2*(perso.x-608),self.y+50*math.sin(self.k)))
        elif self.type==3:
            self.rect.x=self.x-2*(perso.x-608)+50*math.cos(self.k)
            self.rect.y=self.y
            fenetre.blit(self.image, (self.x-2*(perso.x-608)+50*math.cos(self.k),self.y))
        elif self.type==4:
            self.rect.x=self.x-2*(perso.x-608)+50*math.sin(self.k)
            self.rect.y=self.y
            fenetre.blit(self.image, (self.x-2*(perso.x-608)+50*math.sin(self.k),self.y))
        elif self.type==5:
            self.rect.x=self.x-2*(perso.x-608)+50*math.sin(self.k)
            self.rect.y=self.y+50*math.cos(self.k)
            fenetre.blit(self.image, (self.x-2*(perso.x-608)+50*math.sin(self.k),self.y+50*math.cos(self.k)))
        elif self.type==6:
            self.rect.x=self.x-2*(perso.x-608)+50*math.cos(self.k)
            self.rect.y=self.y+50*math.sin(self.k)
            fenetre.blit(self.image, (self.x-2*(perso.x-608)+50*math.cos(self.k),self.y+50*math.sin(self.k)))

class Meuble(pygame.sprite.Sprite):
    def __init__(self,x,valeur):
        pygame.sprite.Sprite.__init__(self)
        self.image=pygame.Surface([95, 51])
        self.image=(pygame.image.load("meuble2.png").convert_alpha()).copy()
        self.image.convert_alpha()
        self.rect=self.image.get_rect()
        self.x=x
        self.y=720-126
        self.deverouille=False
        self.valeur=valeur

    def draw(self,fenetre,perso):
        self.rect.x=self.x-1.5*(perso.x-608)
        self.rect.y=self.y
        fenetre.blit(self.image, (self.x-1.5*(perso.x-608),self.y))


class Verrou(pygame.sprite.Sprite):
    def __init__(self,x,y):
        pygame.sprite.Sprite.__init__(self)
        self.image=pygame.Surface([64, 64])
        self.image=(pygame.image.load("Rond_verrou.png").convert()).copy()
        #self.image.set_alpha(100)
        self.rect=self.image.get_rect()
        self.x=x
        self.y=y
        self.angle_rupt=180*random.random()+180
        self.angle=0
        self.image.set_alpha(int(255*(abs(math.cos((self.angle-self.angle_rupt)*math.pi/360)))))

    def update(self):
        self.image.set_alpha(int(255*(abs(math.cos((self.angle-self.angle_rupt)*math.pi/360)))))


def crochetage(fenetre,nombre_verrous):
    serrure=1
    deverouille=False
    if nombre_verrous==3:
        fond_crochetage=pygame.image.load("fond_crochetage.png").convert_alpha()
    elif nombre_verrous==5:
        fond_crochetage=pygame.image.load("fond_crochetage_coffre.png").convert_alpha()
    verrou=0
    liste_verrous=[]
    if nombre_verrous==5:
        liste_verrous.append(Verrou(318,568))
    liste_verrous.append(Verrou(468,568))
    liste_verrous.append(Verrou(618,568))
    liste_verrous.append(Verrou(768,568))
    if nombre_verrous==5:
        liste_verrous.append(Verrou(918,568))
    pygame.key.set_repeat(30,30)
    while serrure:
        fenetre.blit(fond_crochetage,(0,0))
        if nombre_verrous==5:
            pygame.draw.circle(fond_crochetage,(200,200,200),(350,600),34,1)
        pygame.draw.circle(fond_crochetage,(200,200,200),(500,600),34,1)
        pygame.draw.circle(fond_crochetage,(200,200,200),(650,600),34,1)
        pygame.draw.circle(fond_crochetage,(200,200,200),(800,600),34,1)
        if nombre_verrous==5:
            pygame.draw.circle(fond_crochetage,(200,200,200),(950,600),34,1)
        for lock in liste_verrous:
            fenetre.blit(lock.image,(lock.x,lock.y))
        if verrou>=1:
            pygame.draw.circle(fond_crochetage,(0,255,0),(liste_verrous[0].x+32,liste_verrous[0].y+32),32)
        if verrou>=2:
            pygame.draw.circle(fond_crochetage,(0,255,0),(liste_verrous[1].x+32,liste_verrous[1].y+32),32)
        if verrou>=3:
            pygame.draw.circle(fond_crochetage,(0,255,0),(liste_verrous[2].x+32,liste_verrous[2].y+32),32)
        if verrou>=4:
            pygame.draw.circle(fond_crochetage,(0,255,0),(liste_verrous[3].x+32,liste_verrous[3].y+32),32)
        pygame.display.flip()
        for event in pygame.event.get():
            if event.type == QUIT or event.type == KEYDOWN and event.key == K_ESCAPE:
                serrure = 0
        liste_key = pygame.key.get_pressed()
        if liste_key[K_LEFT]:
            liste_verrous[verrou].angle=(liste_verrous[verrou].angle-1)%360
            liste_verrous[verrou].update()
        if liste_key[K_RIGHT]:
            liste_verrous[verrou].angle=(liste_verrous[verrou].angle+1)%360
            liste_verrous[verrou].update()
        if liste_key[K_SPACE]:
            if abs(math.cos((liste_verrous[verrou].angle-liste_verrous[verrou].angle_rupt)*math.pi/360))>=0.99:
                liste_verrous[verrou].image.set_alpha(0)
                verrou+=1
            if verrou==nombre_verrous:
                serrure = 0
    if verrou==nombre_verrous:
        deverouille=True
    return(deverouille)


def create_scene_phase2(fenetre):
    SPEED=5

    font_score=pygame.font.Font(None,30)
    fondentree=pygame.image.load("fondentree.png").convert_alpha()
    fondcoffre=pygame.image.load("fondcoffre.png").convert_alpha()
    fond0=pygame.image.load("fond0.png").convert_alpha()
    fond1=pygame.image.load("fondmur.png").convert_alpha()
    fond2=pygame.image.load("fondcolonne.png").convert_alpha()
    perso=Perso(3)

    groupe_laser=pygame.sprite.RenderUpdates()
    groupe_laser.add(Laser(1000,600,0))
    groupe_laser.add(Laser(1200,450,0))
    groupe_laser.add(Laser(1200,600,0))
    groupe_laser.add(Laser(2000,550,1))
    groupe_laser.add(Laser(2300,550,2))
    groupe_laser.add(Laser(2900,600,3))
    groupe_laser.add(Laser(3200,610,4))
    groupe_laser.add(Laser(3200,575,4))
    groupe_laser.add(Laser(4000,550,1))
    groupe_laser.add(Laser(4000,550,2))
    groupe_laser.add(Laser(4300,550,6))
    groupe_laser.add(Laser(4900,550,6))
    groupe_laser.add(Laser(4900,550,5))
    groupe_laser.add(Laser(5300,575,3))
    groupe_laser.add(Laser(5300,575,1))
    groupe_laser.add(Laser(5800,575,4))
    groupe_laser.add(Laser(5800,575,2))
    groupe_laser.add(Laser(6500,575,0))
    groupe_laser.add(Laser(6500,575,4))
    groupe_laser.add(Laser(6800,575,2))
    groupe_laser.add(Laser(7200,575,0))
    groupe_laser.add(Laser(7200,575,1))
    groupe_laser.add(Laser(7200,575,2))
    groupe_laser.add(Laser(7200,575,3))
    groupe_laser.add(Laser(7200,575,4))

    groupe_meuble=pygame.sprite.RenderUpdates()
    groupe_meuble.add(Meuble(1300,1000))
    groupe_meuble.add(Meuble(2900,2000))
    groupe_meuble.add(Meuble(3600,3000))
    groupe_meuble.add(Meuble(4800,4000))

    game=True
    x1=0
    x2=0
    xe=0
    liste_collide_meuble=[]
    crochetage_coffre=False
    while game:
        fenetre.blit(fond0,(0,0))
        fenetre.blit(fond1,(-x1,0))
        fenetre.blit(fond1,(1280-x1,0))
        fenetre.blit(fond2,(-x2,0))
        fenetre.blit(fond2,(1280-x2,0))
        fenetre.blit(fondentree,(xe,0))
        fenetre.blit(fondcoffre,(4512-perso.x-30,0))
        fenetre.blit(font_score.render("Score : "+str(perso.score),False,(150,0,0)),(100,100))
        for meuble in groupe_meuble.sprites():
            meuble.draw(fenetre,perso)
        perso.draw(fenetre)
        for las in groupe_laser.sprites():
            las.draw(fenetre,perso)
        pygame.time.Clock().tick(100)
        pygame.display.flip()

        for event in pygame.event.get():
            if event.type == QUIT or event.type == KEYDOWN and event.key == K_ESCAPE:
                game = 0

        perso.dx=0
        liste_key = pygame.key.get_pressed()
        if liste_key[K_LEFT]:
            perso.dx-=SPEED
        if liste_key[K_RIGHT]:
            perso.dx+=SPEED
        if liste_key[K_UP] and perso.saut:
            perso.dy=-SPEED*2
            perso.saut=False
        if liste_key[K_DOWN]:
            perso.dy=SPEED
        if liste_key[K_SPACE]:
            if liste_collide_meuble != []:
                for meuble in liste_collide_meuble:
                    if not(meuble.deverouille):
                        meuble.deverouille=crochetage(fenetre,3)
                    if meuble.deverouille:
                        perso.score+=meuble.valeur
                        meuble.valeur=0
            if perso.x==4512 and not(crochetage_coffre):
                crochetage_coffre=crochetage(fenetre,5)

        if crochetage_coffre:
            perso.score+=10000
            create_scene_phase3(fenetre,perso.score)
            game = 0

        perso.update()

        if perso.dx!=0:
            x1+= 5*perso.dx/abs(perso.dx)
            x2+= 7.5*perso.dx/abs(perso.dx)
            xe-= 7.5*perso.dx/abs(perso.dx)
        if x1>=1280:
            x1-=1280
        elif x1<=0:
            x1+=1280
        if x2>=1280:
            x2-=1280
        elif x2<=0:
            x2+=1280

        liste_collide=pygame.sprite.spritecollide(perso,groupe_laser,False)
        liste_collide_meuble=pygame.sprite.spritecollide(perso,groupe_meuble,False)

        if liste_collide!=[]:
            perso.x=608
            perso.y=581
            x1=0
            x2=0
            xe=0
            perso.score=0
