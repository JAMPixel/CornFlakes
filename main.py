import pygame
from pygame.locals import *
from phase1 import *
from phase2 import *
from phase3 import *

pygame.init()

#Ouverture de la fenetre Pygame (carre : largeur = hauteur)
fenetre = pygame.display.set_mode((1280,720),pygame.FULLSCREEN,32)

##icone
#icone = pygame.image.load(image_icone)
#pygame.display.set_icon(icone)

#Titre
pygame.display.set_caption("L'envol de l'oseille")
fondmenu=pygame.image.load("menu.png").convert_alpha()
menu=1
replay=1
while menu:
    for event in pygame.event.get():
     	#detec touches
        if event.type == QUIT or event.type == KEYDOWN and event.key == K_ESCAPE:
            menu = 0
        elif event.type == KEYDOWN and event.key == K_SPACE:
            replay=create_scene_phase1(fenetre)
            if not(replay):
                menu = 0
    fenetre.blit(fondmenu,(0,0))
    pygame.display.flip()
pygame.quit()
