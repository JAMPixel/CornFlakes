import pygame
from pygame.locals import *
import random
from phase2 import *

class Helico(pygame.sprite.Sprite):
    def __init__(self,vie):
        pygame.sprite.Sprite.__init__(self)
        self.image=pygame.Surface([98, 48])
        self.image=(pygame.image.load("helico.png").convert_alpha()).copy()
        self.image.convert_alpha()
        self.rect=self.image.get_rect()
        self.dx=0
        self.dy=0
        self.x=100
        self.y=320
        self.rect.x=self.x
        self.rect.y=self.y
        self.vie=vie
        self.isblinking=False
        self.k=0
        self.k2=0

    def update(self):
        #self.rect.x+=self.dx
        #self.rect.y+=self.dy
        self.x+=self.dx
        self.y+=self.dy
        if self.x<0:
            self.x=0
        if self.x>1182:
            self.x=1182
        if self.y<0:
            self.y=0
        if self.y>670:
            self.y=670
        self.rect.x=self.x
        self.rect.y=self.y

    def draw(self,fenetre):
        if not(self.isblinking):
            fenetre.blit(self.image,(self.x,self.y))
        else:
            self.k+=1
            if self.k>=5 and self.k<10:
                fenetre.blit(self.image,(self.x,self.y))
            if self.k>10:
                self.k=0
                self.k2+=1
            if self.k2==4:
                self.k2=0
                self.isblinking=0


    def degat(self):
        self.isblinking=True
        self.vie-=1


class Oiseau(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.image=pygame.Surface([28, 17])
        self.frame1=(pygame.image.load("pigeon1.png").convert_alpha()).copy()
        self.frame2=(pygame.image.load("pigeon2.png").convert_alpha()).copy()
        self.image=self.frame1
        self.image.convert_alpha()
        self.rect=self.image.get_rect()
        self.x=1280-28
        self.y=random.randint(0,700)
        self.rect.x=self.x
        self.rect.y=self.y
        self.k=0

    def update(self):
        self.x-=2.5
        self.rect.x=self.x

    def draw(self,fenetre):
        self.k+=0.5
        if self.k<10:
            self.image=self.frame1
        else:
            self.image=self.frame2
            if self.k>=20:
                self.k=0
        fenetre.blit(self.image,(self.x,self.y))



def create_scene_phase1(fenetre):
    SPEED=5

    helico=Helico(3)
    groupe=pygame.sprite.RenderUpdates()
    fond0=pygame.image.load("fond0.png").convert()
    fond1=pygame.image.load("fond1.png").convert_alpha()
    fond2=pygame.image.load("fond2.png").convert_alpha()
    fond3=pygame.image.load("fond3.png").convert_alpha()
    fond4=pygame.image.load("fond4.png").convert_alpha()
    fond5=pygame.image.load("fond5.png").convert_alpha()

    p=0
    t=0
    game=1
    x1=0
    x2=0
    x3=0
    x4=0
    pygame.key.set_repeat(30,30)
    while game:
        if p<0.05:
            p+=0.0001
        else:
            t+=0.001
        if t>=1:
            game=0
            create_scene_phase2(fenetre)
        fenetre.blit(fond0,(0,0))
        fenetre.blit(fond1,(-x1,0))
        fenetre.blit(fond1,(1280-x1,0))
        fenetre.blit(fond2,(-x2,0))
        fenetre.blit(fond2,(1280-x2,0))
        fenetre.blit(fond3,(-x3,0))
        fenetre.blit(fond3,(1280-x3,0))
        fenetre.blit(fond4,(-x4,0))
        fenetre.blit(fond4,(1280-x4,0))
        #fenetre.blit(fond5,(0,0))
        proba=random.random()
        if proba<p:
            oiseau=Oiseau()
            groupe.add(oiseau)
        for bird in groupe.sprites():
            bird.draw(fenetre)
            if bird.x<-28:
                bird.kill()

        helico.draw(fenetre)
        pygame.display.flip()
        if x1==1280:
            x1=0
        if x2==1280:
            x2=0
        if x3==1280:
            x3=0
        if x4>=1280:
            x4-=1280
        x1+=0.25
        x2+=0.5
        x3+=1
        x4+=1.5
        helico.dx=0
        helico.dy=0


        for event in pygame.event.get():
            if event.type == QUIT or event.type == KEYDOWN and event.key == K_ESCAPE:
                game = 0
        liste_key = pygame.key.get_pressed()
        if liste_key[K_LEFT]:
            helico.dx-=SPEED
        if liste_key[K_RIGHT]:
            helico.dx+=SPEED
        if liste_key[K_UP]:
            helico.dy-=SPEED
        if liste_key[K_DOWN]:
            helico.dy+=SPEED
        helico.update()
        #for bird in groupe.sprite():
        liste_collide=pygame.sprite.spritecollide(helico,groupe,False)

        if liste_collide!=[]:
            for bird in liste_collide:
                bird.kill()
            helico.degat()
            if helico.vie==0:
                game=0
        groupe.update()
    return(helico.vie==0)
