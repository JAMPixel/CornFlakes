import pygame
from pygame.locals import *
import random
import math
from phase2 import *
from phasefin import *

class Helico(pygame.sprite.Sprite):
    def __init__(self,score):
        pygame.sprite.Sprite.__init__(self)
        self.image=pygame.Surface([98, 48])
        self.image=(pygame.image.load("helico.png").convert_alpha()).copy()
        self.image.convert_alpha()
        self.rect=self.image.get_rect()
        self.dx=0
        self.dy=0
        self.x=100
        self.y=320
        self.rect.x=self.x
        self.rect.y=self.y
        self.score=score
        self.isblinking=False
        self.k=0
        self.k2=0

    def update(self):
        #self.rect.x+=self.dx
        #self.rect.y+=self.dy
        self.x+=self.dx
        self.y+=self.dy
        if self.x<0:
            self.x=0
        if self.x>1182:
            self.x=1182
        if self.y<0:
            self.y=0
        if self.y>670:
            self.y=670
        self.rect.x=self.x
        self.rect.y=self.y

    def draw(self,fenetre):
        if not(self.isblinking):
            fenetre.blit(self.image,(self.x,self.y))
        else:
            self.k+=1
            if self.k>=5 and self.k<10:
                fenetre.blit(self.image,(self.x,self.y))
            if self.k>10:
                self.k=0
                self.k2+=1
            if self.k2==4:
                self.k2=0
                self.isblinking=0


    def degat(self):
        self.isblinking=True
        self.score-=500


class Oiseau(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.image=pygame.Surface([28, 17])
        self.frame1=(pygame.image.load("pigeon1.png").convert_alpha()).copy()
        self.frame2=(pygame.image.load("pigeon2.png").convert_alpha()).copy()
        self.image=self.frame1
        self.image.convert_alpha()
        self.rect=self.image.get_rect()
        self.x=1280-28
        self.y=random.randint(0,700)
        self.rect.x=self.x
        self.rect.y=self.y
        self.teta=1
        self.k=0

    def update(self):
        self.x-=2.5
        self.rect.x=self.x

    def draw(self,fenetre):
        self.k+=0.5
        if self.k<10:
            self.image=self.frame1
        else:
            self.image=self.frame2
            if self.k>=20:
                self.k=0
        fenetre.blit(self.image,(self.x,self.y))

class Police(pygame.sprite.Sprite):
    def __init__(self,groupe):
        pygame.sprite.Sprite.__init__(self)
        self.image=pygame.Surface([98, 48])
        self.image=(pygame.image.load("helicopolice.png").convert_alpha()).copy()
        self.image.convert_alpha()
        self.rect=self.image.get_rect()
        self.groupe=groupe
        self.x=1280
        self.y_spawn=self.y=random.randint(0,670)
        self.y=self.y_spawn
        self.teta=0

    def update(self):
        self.teta+=0.01
        self.x-=1
        self.y=self.y_spawn+100*math.sin(self.teta*2)
        self.rect.x=self.x
        self.rect.y=self.y

    def draw(self,fenetre):
        fenetre.blit(self.image,(self.x,self.y))


class Projectile(pygame.sprite.Sprite):
    def __init__(self,x,y):
        pygame.sprite.Sprite.__init__(self)
        self.image=pygame.Surface([16, 16])
        self.image=(pygame.image.load("tir.png").convert_alpha()).copy()
        self.image.convert_alpha()
        self.rect=self.image.get_rect()
        self.teta=1
        self.x=x-16
        self.y=y+30
        self.rect.x=self.x
        self.rect.y=self.y

    def update(self):
        self.x-=10
        self.rect.x=self.x

    def draw(self,fenetre):
        fenetre.blit(self.image,(self.x,self.y))


def create_scene_phase3(fenetre,score):
    SPEED=5

    helico=Helico(score)
    font_score=pygame.font.Font(None,30)
    groupe=pygame.sprite.RenderUpdates()
    fond0=pygame.image.load("fond0.png").convert()
    fond1=pygame.image.load("fond1.png").convert_alpha()
    fond2=pygame.image.load("fond2.png").convert_alpha()
    fond3=pygame.image.load("fond3.png").convert_alpha()
    fond4=pygame.image.load("fond4.png").convert_alpha()
    fond5=pygame.image.load("fond5.png").convert_alpha()

    p=0
    t=0
    game=1
    x1=0
    x2=0
    x3=0
    x4=0
    pygame.key.set_repeat(30,30)
    while game:
        if p<0.02:
            p+=0.0001
        else:
            t+=0.0002
        if t>=1:
            game=0
            create_scene_phase_fin(fenetre,helico.score)
        fenetre.blit(fond0,(0,0))
        fenetre.blit(fond1,(-x1,0))
        fenetre.blit(fond1,(1280-x1,0))
        fenetre.blit(fond2,(-x2,0))
        fenetre.blit(fond2,(1280-x2,0))
        fenetre.blit(fond3,(-x3,0))
        fenetre.blit(fond3,(1280-x3,0))
        fenetre.blit(fond4,(-x4,0))
        fenetre.blit(fond4,(1280-x4,0))
        fenetre.blit(font_score.render("Score : "+str(helico.score),False,(150,0,0)),(100,100))
        #fenetre.blit(fond5,(0,0))
        proba=random.random()
        if proba<p:
            oiseau=Oiseau()
            groupe.add(oiseau)
        proba=random.random()
        if proba<p/10:
            groupe.add(Police(groupe))
        for bird in groupe.sprites():
            bird.draw(fenetre)
            if type(bird)==type(Police(groupe)):
                if int(bird.teta*100)%100==0:
                    groupe.add(Projectile(bird.x,bird.y))
            if bird.x<-100:
                bird.kill()

        helico.draw(fenetre)
        pygame.display.flip()
        if x1==1280:
            x1=0
        if x2==1280:
            x2=0
        if x3==1280:
            x3=0
        if x4>=1280:
            x4-=1280
        x1+=0.25
        x2+=0.5
        x3+=1
        x4+=1.5
        helico.dx=0
        helico.dy=0


        for event in pygame.event.get():
            if event.type == QUIT or event.type == KEYDOWN and event.key == K_ESCAPE:
                game = 0
        liste_key = pygame.key.get_pressed()
        if liste_key[K_LEFT]:
            helico.dx-=SPEED
        if liste_key[K_RIGHT]:
            helico.dx+=SPEED
        if liste_key[K_UP]:
            helico.dy-=SPEED
        if liste_key[K_DOWN]:
            helico.dy+=SPEED
        helico.update()
        #for bird in groupe.sprite():
        liste_collide=pygame.sprite.spritecollide(helico,groupe,False)

        if liste_collide!=[]:
            for bird in liste_collide:
                bird.kill()
            helico.degat()
        groupe.update()
